# nadica: The Coolest Web Store Ever #

Bellow are the necessary steps to get you up and running. Have fun! :-)

### Setup ###

* This is a Wordpress project, so the first thing to do is to install and
  configure Apache Server, PHP and MySQL. There are plenty of tutorials on the
  web for doing this on many platforms (e.g.: use XAMPP). Just google it!
* After you're set, clone this repository under your public Apache directory
  (on Mac OS X, it might be something like "~/Sites/~<username>").
* Run the SQL script "create\_db.sql" under the folder "sql" in order to create
  the database. Then, run "nadica.sql" in order to fill out the database.
* You're done!

### Important Notes ###

* The site is under the "htdocs" folder, so point your web browser to there.
* Before comitting, make sure to export the current state of the database to
  the file "nadica.sql" (you'll may also have to re-run it after a "git pull").
  In order to do that, exec `mysqldump` from the "sql" directory as follows:
  `mysqldump -u <username> -p <password> nadica > nadica.sql`.
