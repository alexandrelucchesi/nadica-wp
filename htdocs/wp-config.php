<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// Use these settings on the local server.
if (file_exists(dirname(__FILE__) . '/wp-config-local.php')) {
  include(dirname( __FILE__ ) . '/wp-config-local.php');
} else { // Otherwise use the below settings (on live server).
  /** The name of the database for WordPress */
  define('DB_NAME', 'nadica');

  /** MySQL database username */
  define('DB_USER', 'root');

  /** MySQL database password */
  define('DB_PASSWORD', '');

  /** MySQL hostname */
  define('DB_HOST', 'localhost');

  /** Overwrites the database to save keep editing the DB. */
  define('WP_HOME', 'http://nadica.com.br');
  define('WP_SITEURL', 'http://nadica.com.br');

  /**
   * For developers: WordPress debugging mode.
   *
   * Change this to true to enable the display of notices during development.
   * It is strongly recommended that plugin and theme developers use WP_DEBUG
   * in their development environments.
   */
  define('WP_DEBUG', false);
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', 'utf8_general_ci');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!@5)CG,cn|;:[|,`/({SS{o:~wV|T!-zF1YB}7GhB<aM}l|yoV0E2S.B0P!dv5$K');
define('SECURE_AUTH_KEY',  '^.N34;R0+*>%2AEQ{7S)e?sS/1vvw%%$r:F 5kp#u&Y r%j!F!2glm#DHeC+,t<)');
define('LOGGED_IN_KEY',    'W~hOdVgVx*rzllHQO)qECk<Q]PC|xK+r}.!!-2RV]0iy/sNU)B_|pD6]L0XnHpmA');
define('NONCE_KEY',        '9>7CT$Xx}D-gt<-uPwWX>:qo:s/fHO3Tf>0GVG} ST%8^}ib.i{O=K2L}o1Fss{B');
define('AUTH_SALT',        '{fgD-QJNL?r-60W>mYD7~_9tlK}X+XQxw.fb7=)Kas{cX?tKw-_*9EhIUztlz`0+');
define('SECURE_AUTH_SALT', '<zqyXO_sJ-GB-)r@4O<=E{vNqS)XCX=?yC@k>aljs5{AoyTc6q7{*SG9Wd(UL>q[');
define('LOGGED_IN_SALT',   'z)SJs#|l=H+O`y<P3Ey*#q; ae.+Ihn~Zuxmm#?|1nhWM>zt|/4Ed_Y-1H;G&oa;');
define('NONCE_SALT',       'hW)jXU<Wy$|?+`y )vDUu|xT}S+a~aS]WLZv)EQYbn9Sot|%_w4~Vd&U(F`p{<MY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

