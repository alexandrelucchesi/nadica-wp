<?php

/* Set up parent/child stuff. */
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}


/* Remove Storefront credit from footer. */
add_action( 'init', 'custom_remove_footer_credit', 10 );

function custom_remove_footer_credit () {
    remove_action( 'storefront_footer', 'storefront_credit', 20 );
    add_action( 'storefront_footer', 'custom_storefront_credit', 20 );
} 

function custom_storefront_credit() {
    ?>
    <div class="site-info">
        &copy; <?php echo get_bloginfo( 'name' ) . ' ' . get_the_date( 'Y' ); ?>
    </div><!-- .site-info -->
    <?php
}


/* Remove Storefront homepage title. */ 
function storefront_page_header() {

    if ( !is_front_page() ) { ?>
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title" itemprop="name">', '</h1>' ); ?>
        </header><!-- .entry-header -->
		<?php }

}


